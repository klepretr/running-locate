package fr.lepretre.kevin.runninglocate;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.content.ServiceConnection;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.File;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivityTAG";
    private boolean stateLocationLogger = false;
    private String mLogFilepath = "position.log";

    private final int MAX_VAL_SAMPLING_SEC = 200;
    private final int DEFAULT_VAL_SAMPLING_SEC = 10;
    private final int MIN_VAL_SAMPLING_SEC = 1;
    private int mSamplingVal = DEFAULT_VAL_SAMPLING_SEC*1000;

    private final int DELAY_UPDATE_INFOS = 1000;
    private Timer mUpdateInfosTimer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        int[] ressouces = new int[]{R.string.hello_world0,R.string.hello_world1,R.string.hello_world2,R.string.hello_world3};
        TextView textView = (TextView) findViewById(R.id.txt_hello_world);
        Random r = new Random();
        textView.setText(
                getString(
                        ressouces[(r.nextInt(ressouces.length))]
                )
        );

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(stateLocationLogger){
                    Intent intent = new Intent(getApplicationContext(), LocationLogger.class);
                    stopService(intent);
                }else{
                    Intent intent = new Intent(MainActivity.this, LocationLogger.class);
                    intent.putExtra("sampling", mSamplingVal);
                    intent.setAction(ILocationLoggerInterface.class.getName());
                    startService(intent);
                    bindService(intent, mConnection, 0);

                }
            }
        });

        Button btn_list_elem = (Button) findViewById(R.id.btn_list_elem);
        btn_list_elem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ListElemActivity.class);
                startActivity(intent);
            }
        });

        Button btn_delete_log = (Button) findViewById(R.id.btn_delete_log);
        btn_delete_log.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File file = new File(getApplicationContext().getFilesDir(), mLogFilepath);
                file.delete();
            }
        });

        final TextView txt_val_seek = (TextView) findViewById(R.id.txt_val_seek);
        txt_val_seek.setText(
                String.format(getString(R.string.format_val_seeker_sampling), DEFAULT_VAL_SAMPLING_SEC)
        );

        SeekBar seek_bar_sampling = (SeekBar) findViewById(R.id.seek_sampling_bar);
        seek_bar_sampling.setMax(MAX_VAL_SAMPLING_SEC); seek_bar_sampling.setMin(MIN_VAL_SAMPLING_SEC);
        seek_bar_sampling.setProgress(DEFAULT_VAL_SAMPLING_SEC);
        seek_bar_sampling.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mSamplingVal = seekBar.getProgress() * 1000;
                if(mILocationLoggerInterface != null){
                    try {
                        mILocationLoggerInterface.setSamplingDuration(mSamplingVal);
                    } catch (RemoteException e) {
                        Log.e(TAG, "onStopTrackingTouch: impossible to communicate with Logger service", e);
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // Nothing to do
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                txt_val_seek.setText(
                        String.format(getString(R.string.format_val_seeker_sampling), progress)
                );
            }
        });

        Button btn = (Button) findViewById(R.id.btn_reset_alarm);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mILocationLoggerInterface != null){
                    try {
                        mILocationLoggerInterface.rearmedLocDetection();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    ILocationLoggerInterface mILocationLoggerInterface;
    public ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            mILocationLoggerInterface = ILocationLoggerInterface.Stub.asInterface(service);

            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
            fab.setImageResource(android.R.drawable.ic_media_pause);
            stateLocationLogger = true;

            mUpdateInfosTimer = new Timer();
            mUpdateInfosTimer.schedule(new UpdateInfosTask(),1, DELAY_UPDATE_INFOS);

        }

        public void onServiceDisconnected(ComponentName className) {
            Log.e(TAG, "Service has unexpectedly disconnected");
            mILocationLoggerInterface = null;

            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
            fab.setImageResource(android.R.drawable.ic_media_play);
            stateLocationLogger = false;

            if(mUpdateInfosTimer != null){
                mUpdateInfosTimer.cancel();
            }
            TextView txt_realtime_running = (TextView) findViewById(R.id.txt_realtime_running);
            txt_realtime_running.setText(R.string.placeholder_realtime_running);
        }
    };

    public class UpdateInfosTask extends TimerTask{
        @Override
        public void run() {
            Log.d(TAG, "run: Start update running data");
            if(mILocationLoggerInterface == null){
                Log.i(TAG, "run: no LocationLoggerServiceInterface available");
                return;
            }

            try {
                long running_time = mILocationLoggerInterface.getRunningTime();
                float running_distance = mILocationLoggerInterface.getRunningDistance();
                final TextView txt_realtime_running = (TextView) findViewById(R.id.txt_realtime_running);
                double distance = running_distance/1000.0;
                String format_duration = String.format("%d:%02d:%02d", running_time / 3600, (running_time % 3600) / 60, (running_time % 60));
                final String new_text = String.format(getString(R.string.format_realtime_running), distance, format_duration);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        txt_realtime_running.setText(new_text);
                    }
                });

            } catch (RemoteException e) {
                Log.e(TAG, "run: impossible to communicate with LocationLoggerService");
                return;
            }
        }
    }
}
