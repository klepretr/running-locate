package fr.lepretre.kevin.runninglocate;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class LocationLogger extends Service {
    private String TAG = "LocationLoggerService";

    private int mSamplingLocation;
    private final int DEFAULT_SAMPLING_LOCATION = 10000;
    private final int MAX_SAMPLING_LOCATION = 60000;
    private final int MIN_SAMPLING_LOCATION = 1000;

    private LocalDateTime mStartingService;
    private float mDistance;

    private final float DEFAULT_RADIUS_LOC_DETECT = 100;
    private float mRadiusLocDetect = DEFAULT_RADIUS_LOC_DETECT;
    private Location mLocDetect = null;
    private int mStateDetect = -1;

    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;
    private LocationGetter mLocationGetter;

    private LocationManager mLocationManager;

    private String mLogFilename;
    private final String DEFAULT_FILENAME_LOG_LOCATION = "position.log";

    private FileOutputStream mLogFile;

    private boolean mStateLocationProvider = false;

    Location mLastLocation = null;
    private double mLastLatitude = -1;
    private double mLastLongitude = -1;
    private double mLastAltitude = -1;

    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            Log.i(TAG, "onLocationChanged: new location available");
            mStateLocationProvider = true;
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            // Nothing to do
        }

        @Override
        public void onProviderEnabled(String provider) {
            // Nothing to do
        }

        @Override
        public void onProviderDisabled(String provider) {
            Toast.makeText(getApplicationContext(), getString(R.string.content_provider_location_need), Toast.LENGTH_LONG)
                    .show();
            mStateLocationProvider = false;
            stopSelf();
        }
    };

    private final class LocationGetter implements Runnable {
        @Override
        public void run() {
            Log.i(TAG, "run: running getting location");

            if(mStateLocationProvider){
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "handleMessage: Location permission not set, impossible to access data ");
                    Toast.makeText(getApplicationContext(), getString(R.string.content_error_permission_location), Toast.LENGTH_LONG).show();
                    stopSelf();
                    return;
                }
                Location loc = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                Date now = new Date();

                CharSequence strDate = ZonedDateTime.now().format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);


                if(mLastLocation != null){
                    mDistance += mLastLocation.distanceTo(loc);
                }

                if(mLocDetect != null && mStateDetect == 0){
                    if(loc.distanceTo(mLocDetect) <= mRadiusLocDetect){
                        Intent intent = new Intent();
                        intent.setAction("fr.lepretre.kevin.runninglocate.LOCATE_DETECT");
                        intent.setPackage("fr.lepretre.kevin.runninglocate");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        mStateDetect = 1;//ALERT STATE
                        startActivity(intent);
                    }
                }


                mLastLocation = loc;
                mLastLongitude = loc.getLongitude(); mLastLatitude = loc.getLatitude(); mLastAltitude = loc.getAltitude();

                String entry = String.format(getString(R.string.format_log_loc_file), strDate, loc.getLatitude(), loc.getLongitude(), loc.getAltitude());
                try {
                    mLogFile.write(entry.getBytes());
                } catch (IOException e) {
                    Log.e(TAG, "handleMessage: Impossible to write in log file");
                    Toast.makeText(getApplicationContext(), getString(R.string.content_error_write_log_loc_file), Toast.LENGTH_LONG).show();
                    stopSelf();
                    return;
                }
            }
            mServiceHandler.postDelayed(this, mSamplingLocation);
        }
    }

    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            Log.i(TAG, "handleMessage: i'm alive");

            mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Log.e(TAG, "handleMessage: Location permission not set, impossible to access data ");
                Toast.makeText(getApplicationContext(), getString(R.string.content_error_permission_location), Toast.LENGTH_LONG).show();
                stopSelf();
                return;
            }

            File logFile = new File(getBaseContext().getFilesDir(),mLogFilename);
            Log.i(TAG, "handleMessage: opening file "+logFile);
            if(!logFile.exists()){
                try {
                    logFile.createNewFile();
                } catch (IOException e) {
                    Log.e(TAG, "handleMessage: Impossible to create log file on storage space");
                    Toast.makeText(getApplicationContext(), getString(R.string.content_error_create_log_loc_file), Toast.LENGTH_LONG).show();
                    stopSelf();
                    return;
                }
            }
            try {
                mLogFile = openFileOutput(mLogFilename, Context.MODE_PRIVATE | Context.MODE_APPEND);
            } catch (FileNotFoundException e) {
                Log.e(TAG, "handleMessage: Impossible to open log file");
                Toast.makeText(getApplicationContext(), getString(R.string.content_error_open_log_loc_file), Toast.LENGTH_LONG).show();
                stopSelf();
                return;
            }

            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, mSamplingLocation, 0, mLocationListener);
        }
    }

    public LocationLogger() {
        HandlerThread thread = new HandlerThread("ServiceStartArguments", Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
        mLocationGetter = new LocationGetter();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //Toast.makeText(this, "service starting", Toast.LENGTH_SHORT).show();

        Log.i(TAG, "onStartCommand: time is running out");

        mSamplingLocation = intent.getIntExtra("sampling", DEFAULT_SAMPLING_LOCATION);
        mLogFilename = intent.getStringExtra("filename");
        if(mLogFilename == null){
            mLogFilename = DEFAULT_FILENAME_LOG_LOCATION;
        }

        mLocDetect = new Location("");
        mLocDetect.setLatitude(47.082608d);
        mLocDetect.setLongitude(2.4162847d);
        mStateDetect = 0; //ARMED DETECTION

        Log.i(TAG, "onStartCommand: sampling > "+mSamplingLocation+" logfile > "+mLogFilename);

        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);

        mServiceHandler.postDelayed(mLocationGetter, mSamplingLocation);

        mStartingService = LocalDateTime.now();
        mDistance = 0;

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "onDestroy: service done");
        mServiceHandler.removeCallbacks(mLocationGetter);
        mLocationManager.removeUpdates(mLocationListener);
        
        //Toast.makeText(this, "service done", Toast.LENGTH_SHORT).show();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return (IBinder) mBinder;
    }

    private final ILocationLoggerInterface mBinder = new ILocationLoggerInterface.Stub() {
        public boolean getLocationState(){
            return mStateLocationProvider;
        }

        public double getLastLongitude(){
            return mLastLongitude;
        }

        public double getLastLatitude(){
            return mLastLatitude;
        }

        public double getLastAltitude(){
            return mLastAltitude;
        }

        public long getRunningTime(){
            if(mStartingService != null){
                return mStartingService.until(LocalDateTime.now(), ChronoUnit.SECONDS);
            }
            return 0;
        }

        public float getRunningDistance(){ return mDistance; }

        public void setSamplingDuration(int duration){
            Log.i(TAG, "setSamplingDuration: change value of sampling duration to " + duration );
            if( duration >= MIN_SAMPLING_LOCATION && duration <= MAX_SAMPLING_LOCATION){
                mSamplingLocation = duration;
                mServiceHandler.removeCallbacks(mLocationGetter);
                mServiceHandler.postDelayed(mLocationGetter, mSamplingLocation);
            }
        }

        public void rearmedLocDetection(){
            mStateDetect = 0;
        }

        public void stopLocDetection(){
            mStateDetect = -1;
        }
    };
}
