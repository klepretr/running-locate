package fr.lepretre.kevin.runninglocate;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

public class ListElemActivity extends AppCompatActivity {
    private static final String TAG = "ListElemActivity";
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private ArrayList<String> mData;

    private FileInputStream mLogFile;

    private String mLogFilename;
    private final String DEFAULT_FILENAME_LOG_LOCATION = "position.log";

    protected ArrayList<String> getLogPosition(){
        ArrayList lines = new ArrayList();
        try {
            mLogFile = openFileInput(mLogFilename);
            InputStreamReader inputStreamReader = new InputStreamReader(mLogFile);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                lines.add(line);
            }
            Collections.reverse(lines);

        } catch (FileNotFoundException e) {
            Log.e(TAG, "onCreate: impossible to open specified log file");
            mAdapter = new PositionGPSAdapter(
                    new ArrayList<String>()
            );
        } catch (IOException e) {
            Log.e(TAG, "onCreate: error I/O during reading log file");
        }

        return lines;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mLogFilename = DEFAULT_FILENAME_LOG_LOCATION;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_elem);

        mRecyclerView = (RecyclerView) findViewById(R.id.lv_pos_gps);

        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mData = getLogPosition();

        mAdapter = new PositionGPSAdapter(
                mData
        );

        mRecyclerView.setAdapter(mAdapter);


        final SwipeRefreshLayout swp_refresh_ly = (SwipeRefreshLayout) findViewById(R.id.swp_refresh_ly);
        swp_refresh_ly.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mData = getLogPosition();

                mAdapter = new PositionGPSAdapter(
                        mData
                );
                mRecyclerView.setAdapter(mAdapter);

                swp_refresh_ly.setRefreshing(false);
            }
        });


    }

    public class PositionGPSAdapter extends RecyclerView.Adapter<PositionGPSAdapter.PositionViewHolder> {
        private ArrayList<String> mDataset;

        @NonNull
        @Override
        public PositionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {

            View layout = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_view_position_elem, parent, false);


            PositionViewHolder vh = new PositionViewHolder(layout);
            return vh;
        }

        @Override
        public void onBindViewHolder(@NonNull PositionViewHolder viewHolder, int i) {
            String logline = mDataset.get(i);

            final String[] splitline = logline.split(";");


            if(splitline.length == 4){
                ZonedDateTime zdt = ZonedDateTime.parse(splitline[0], DateTimeFormatter.ISO_OFFSET_DATE_TIME);

                String format_datetime = zdt.format(DateTimeFormatter.ofPattern("EEEE d LLLL H:m:s"));
                String format_latitude = String.format(getString(R.string.format_latitude), Float.parseFloat(splitline[1].replace(',','.')));
                String format_longitude = String.format(getString(R.string.format_longitude), Float.parseFloat(splitline[2].replace(',','.')));

                viewHolder.mDatetime.setText(format_datetime);
                viewHolder.mLatitude.setText(format_latitude);
                viewHolder.mLongitude.setText(format_longitude);

                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String uri = "geo:"+splitline[1].replace(',','.')+","+splitline[2].replace(',','.');
                        Log.i(TAG, "onClick: uri > "+uri);
                        Uri gmmIntentUri = Uri.parse(uri);
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps");
                        startActivity(mapIntent);

                    }
                });
            }


        }

        @Override
        public int getItemCount() {
            return mDataset.size();
        }

        public PositionGPSAdapter(ArrayList<String> pDataset){
            mDataset = pDataset;
        }

        public class PositionViewHolder extends RecyclerView.ViewHolder {
            public TextView mDatetime;
            public TextView mLatitude;
            public TextView mLongitude;

            public PositionViewHolder(@NonNull View v) {
                super(v);
                mDatetime = v.findViewById(R.id.txt_datetime);
                mLatitude = v.findViewById(R.id.txt_latitude);
                mLongitude = v.findViewById(R.id.txt_longitude);
            }

        }
    }



    }
