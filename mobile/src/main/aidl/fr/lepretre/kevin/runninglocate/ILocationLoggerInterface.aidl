// ILocationLoggerInterface.aidl
package fr.lepretre.kevin.runninglocate;

// Declare any non-default types here with import statements

interface ILocationLoggerInterface {
    boolean getLocationState();
    double getLastLongitude();
    double getLastLatitude();
    double getLastAltitude();

    long getRunningTime();

    float getRunningDistance();

    void stopLocDetection();
    void rearmedLocDetection();

    void setSamplingDuration(int duration);
}
