# TP3 : Application de course à pied

#### Kévin Leprêtre

### Liste des fonctionnalités

- Lancer et arrêter un service

  - gère toute la récupération des positions GPS
  - journalise les données dans un fichier

- Activité de visualisation des positions

  - utilise un `RecyclerView` pour afficher les données issues du fichier de log
  - peut-être mise à jour grâce à un swipe down
  - clic sur les éléments provoque un `intent` géographique

- Changement d'intervalle de prise de position

  - utilise une `SeekBar` pour récupérer la nouvelle valeur de l'intervalle en seconde souhaité
  - transmet grâce à une interface décrite en `AIDL` l'ordre au service

- Information en direct

  - utilisation de méthode du service pour récupérer les données issus du service sur la course en cours

- Alerte de proximité

  - déclanché lors de l'apporche d'une localisation codé en dur



  Par manque de temps, je n'ai pas réussi à implémenter la partie montre connectée, car je n'ai pas réussi à associé l'émulateur de montre à mon téléphone.

### Démonstration

- Un émulateur de position GPS effectue un trajet de la Place Malus à Bourges jusqu'à l'INSA
- Le lieu détecté lors de la démonstration est le 88 Boulevard Lahitolle à Bourges dans un rayon de 100 m
- Le service log les positions toutes les secondes dans un premier temps puis toutes les 10 secondes après modification sans éteindre le service
- Une alarme de localisation déclanche une activité *fullscreen* qui informe l'utilisateur même en dehors de l'application
- Les positions sont loggées dans un fichier qui peut-être éffacé grâce au bouton corbeille
- L'activité qui liste les positions GPS peut-être rafraichie en swipant vers le bas et le clic sur élement ouvre l'application Google Maps au bon endroit
- Des informations sur la course courante sont disponibles sur l'activité principale

![Demo GIF](./doc/img/demo_android.gif)

### Prérequis pour le projet

- API 28 : Android Pie (9.0) *Testé sur Nexus 5 sous Android 9*
